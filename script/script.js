
function loadData() {

    var $body = $('body');
    var $wikiElem = $('#wikipedia-links');
    var $nytHeaderElem = $('#nytimes-header');
    var $nytElem = $('#nytimes-articles');
    var $greeting = $('#greeting');

    // clear out old data before new request
    $wikiElem.text("");
    $nytElem.text("");

    var strtName = $('#street').val();
    
    var cityName = $('#city').val();
    
    var address = strtName +','+ cityName;

    var imageUrl = 'http://maps.googleapis.com/maps/api/streetview?size=600x300&location=' + address + '';
    console.log(imageUrl);

    $body.append('<img class="bgimg" src="' + imageUrl + '">')

    // load streetview

    var nytimesUrl  =   'http://api.nytimes.com/svc/search/v2/articlesearch.json?q='+ cityName +'&sort=newest&api-key=cb39ce4878644fcfa730a97775f320e5';

    var wikiUrl = 'http://en.wikipedia.org/w/api.php?action=opensearch&search=' + cityName + '&format=json&callback=wikiCallback';

    $.getJSON(nytimesUrl , function(data) 
    {
        console.log(data);
        $nytHeaderElem.text(cityName);

        var articles    =   data.response.docs;
       
        for (var i = 0; i < articles.length; i++) {
            
            var article = articles[i];
            
            $nytElem.append('<li class="article">'+'<a href=" '+ article.web_url +' ">' + article.headline.main + '</a>'
                            +'<p>' + article.snippet + '</p>' + '</li>');
        };

    }).error(function(e){
        $nytHeaderElem.text('your search failed');
    });



    var wikiTimeout = setTimeout(function() {
        
        $wikiElem.text("your search failed");
    
    }, 8000);

    $.ajax({
        url: wikiUrl,
        dataType: "jsonp",
        success: function(response) {

            var articleList = response[1];

            for(var i = 0; i < articleList.length; i++) {

                articleStr = articleList[i];
                var wikiLink = 'http://en.wikipedia.org/wiki/' + articleStr;
                $wikiElem.append('<li><a href="' + wikiLink + '">' + articleStr + '</a></li>');
            };

            clearTimeout(wikiTimeout);
        }

    });

    // YOUR CODE GOES HERE!

    return false;
};

$('#form-container').submit(loadData);
